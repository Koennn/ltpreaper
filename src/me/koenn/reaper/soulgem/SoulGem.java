package me.koenn.reaper.soulgem;

import me.koenn.reaper.references.ItemReferences;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

import static org.bukkit.ChatColor.translateAlternateColorCodes;
import static org.bukkit.Material.EMERALD;

public class SoulGem extends ItemStack {

    private SoulGemType gemType;

    public SoulGem(SoulGemType gemType) {
        super(EMERALD);
        this.gemType = gemType;
        ItemMeta meta = this.getItemMeta();
        meta.setDisplayName(ItemReferences.SOUL_GEM_NAME.replace("{type}", gemType.toString()));
        ArrayList<String> lore = new ArrayList<>();
        lore.add(translateAlternateColorCodes('&', ItemReferences.SOUL_GEM_LORE));
        meta.setLore(lore);
        this.setItemMeta(meta);
    }

    public SoulGem(ItemStack soulGem) {
        try {
            this.gemType = SoulGemType.parseString(soulGem.getItemMeta().getDisplayName());
        } catch (IllegalArgumentException ex) {
            throw new IllegalArgumentException("Item is not a SoulGem.");
        }
    }

    public SoulGemType getGemType() {
        return gemType;
    }
}
