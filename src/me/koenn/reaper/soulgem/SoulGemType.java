package me.koenn.reaper.soulgem;

import me.koenn.core.misc.FancyString;
import org.bukkit.ChatColor;

import static org.bukkit.ChatColor.*;

@SuppressWarnings("unused")
public enum SoulGemType {

    SIMPLE(WHITE, 1), RARE(BLUE, 2), LEGENDARY(GOLD, 4);

    final ChatColor color;
    final int multiplier;

    SoulGemType(ChatColor color, int multiplier) {
        this.color = color;
        this.multiplier = multiplier;
    }

    public static SoulGemType parseString(String string) {
        for (SoulGemType type : values()) {
            if (string.startsWith(type.toString())) {
                return type;
            }
        }
        throw new IllegalArgumentException("Unknown SoulGemType '" + string + "'.");
    }

    public static SoulGemType fromColor(String stringWithColor) {
        for (SoulGemType type : values()) {
            if (stringWithColor.startsWith(type.getColor())) {
                return type;
            }
        }
        throw new IllegalArgumentException("Unknown SoulGemType color '" + stringWithColor + "'.");
    }

    public String getColor() {
        return color + "" + BOLD;
    }

    public int getMultiplier() {
        return multiplier;
    }

    @Override
    public String toString() {
        return this.getColor() + new FancyString(this.name()).toString();
    }
}
