package me.koenn.reaper.commands;

import me.koenn.core.command.Command;
import me.koenn.core.player.CPlayer;
import me.koenn.reaper.references.Messages;

public class TokensCommand extends Command {

    public TokensCommand() {
        super("tokens", "/tokens");
    }

    @Override
    public boolean execute(CPlayer cPlayer, String[] strings) {
        if (!cPlayer.hasField("reapertokens")) {
            cPlayer.set("reapertokens", 0);
        }
        cPlayer.sendMessage((Messages.PREFIX + Messages.TOKENS).replace("{tokens}", cPlayer.get("reapertokens")));
        return true;
    }
}
