package me.koenn.reaper.util;

import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.effect.AtomEffect;
import de.slikey.effectlib.effect.LineEffect;
import de.slikey.effectlib.effect.SphereEffect;
import de.slikey.effectlib.util.DynamicLocation;
import de.slikey.effectlib.util.ParticleEffect;
import me.koenn.reaper.LTPReaper;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.List;

public class ParticleUtil {

    public static void playBeams(int duration, Location center, List<LivingEntity> blazes) {
        for (LivingEntity blaze : blazes) {
            playLineEffect(blaze, center, duration);
        }
    }

    private static void playLineEffect(LivingEntity target, Location center, int duration) {
        LineEffect effect = new LineEffect(new EffectManager(LTPReaper.getInstance()));
        effect.particle = ParticleEffect.FLAME;
        effect.iterations = duration - 20;
        effect.particleCount = 1;
        effect.particles = 30;
        effect.speed = 0.004F;
        effect.setDynamicOrigin(new DynamicLocation(target));
        effect.setDynamicTarget(new DynamicLocation(center.clone().add(0, -0.3, 0)));
        effect.start();
    }

    public static void playApplyEffect(Player player) {
        SphereEffect effect = new SphereEffect(new EffectManager(LTPReaper.getInstance()));
        effect.particle = ParticleEffect.SPELL;
        effect.yOffset = 0.4;
        effect.speed = 0.3F;
        effect.particleCount = 5;
        effect.radius = 1;
        effect.iterations = 1;
        effect.setDynamicOrigin(new DynamicLocation(player.getLocation().add(0, 0.5, 0)));
        effect.setDynamicTarget(new DynamicLocation(player.getLocation().add(0, 0.5, 0)));
        effect.start();
    }

    public static void startPassiveParticle() {
        playPassiveParticle(0);
    }

    private static void playPassiveParticle(double rotation) {
        AtomEffect effect = new AtomEffect(new EffectManager(LTPReaper.getInstance()));
        effect.radius = 3;
        effect.particleNucleus = ParticleEffect.ENCHANTMENT_TABLE;
        effect.particleOrbital = ParticleEffect.ENCHANTMENT_TABLE;
        effect.rotation = rotation;
        effect.setDynamicOrigin(new DynamicLocation(LTPReaper.reaper.getLocation()));
        effect.setDynamicTarget(new DynamicLocation(LTPReaper.reaper.getLocation()));
        effect.iterations = 10;
        effect.callback = () -> playPassiveParticle(rotation + 10);
        if (!LTPReaper.reaper.isInUse()) {
            effect.start();
        }
    }
}
