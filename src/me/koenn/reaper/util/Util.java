package me.koenn.reaper.util;

import me.koenn.core.misc.LocationHelper;
import me.koenn.core.misc.LoreHelper;
import me.koenn.reaper.LTPReaper;
import me.koenn.reaper.reaper.Pylon;
import me.koenn.reaper.reaper.Reaper;
import me.koenn.reaper.references.ItemReferences;
import me.koenn.reaper.soulgem.SoulGemType;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static org.bukkit.ChatColor.GOLD;
import static org.bukkit.ChatColor.WHITE;
import static org.bukkit.Material.BOW;

@SuppressWarnings("BooleanMethodIsAlwaysInverted")
public class Util {

    public static ItemStack getReaperTool() {
        ItemStack reaperTool = new ItemStack(Material.BLAZE_ROD);
        ItemMeta meta = reaperTool.getItemMeta();
        meta.setDisplayName(WHITE + "Reaper Tool");
        meta.setLore(getLore(Mode.PYLON));
        reaperTool.setItemMeta(meta);
        return reaperTool;
    }

    public static List<String> getLore(Mode mode) {
        List<String> lore = new ArrayList<>();
        lore.add(GOLD + "Mode: " + mode.name());
        lore.add(GOLD + "Click to select");
        return lore;
    }

    public static boolean isValidWeapon(Material material) {
        String materialString = material.name();
        return materialString.endsWith("SWORD") || materialString.endsWith("AXE") || material.equals(BOW);
    }

    public static void setKills(ItemStack item, int kills) {
        ItemMeta meta = item.getItemMeta();
        if (!meta.hasLore()) {
            return;
        }

        String loreLine = meta.getLore().get(0);
        SoulGemType gemType;
        try {
            gemType = SoulGemType.fromColor(loreLine);
        } catch (IllegalArgumentException ex) {
            return;
        }

        meta.setLore(LoreHelper.makeLore(ItemReferences.SOUL_GEM_KILLS
                .replace("{color}", gemType.getColor())
                .replace("{kills}", String.valueOf(kills))));
        item.setItemMeta(meta);
    }

    public static boolean hasSoulGem(ItemStack item) {
        if (!item.getItemMeta().hasLore()) {
            return false;
        }
        String loreLine = item.getItemMeta().getLore().get(0);
        try {
            SoulGemType.fromColor(loreLine);
        } catch (IllegalArgumentException ex) {
            return false;
        }

        try {
            Integer.parseInt(loreLine.split(" ")[1]);
        } catch (NumberFormatException ex) {
            return false;
        }

        return true;
    }

    public static int getKills(ItemStack item) {
        try {
            return Integer.parseInt(item.getItemMeta().getLore().get(0).split(" ")[1]);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    @SuppressWarnings("Convert2streamapi")
    public static Reaper loadReaper() {
        try {
            JSONObject reaperObject = LTPReaper.getJsonManager().getBody();

            Location location = LocationHelper.fromString((String) reaperObject.get("location"));

            JSONArray pylonArray = (JSONArray) reaperObject.get("pylons");
            List<Pylon> pylons = new ArrayList<>();
            for (Object pylon : pylonArray) {
                pylons.add(new Pylon(LocationHelper.fromString((String) pylon)));
            }

            JSONArray fireArray = (JSONArray) reaperObject.get("fireplaces");
            List<Location> fireplaces = new ArrayList<>();
            for (Object fireplace : fireArray) {
                fireplaces.add(LocationHelper.fromString((String) fireplace));
            }

            if (pylonArray.isEmpty() || fireArray.isEmpty()) {
                return null;
            }

            return new Reaper(location, pylons, fireplaces);

        } catch (Exception ex) {
            return null;
        }
    }
}
