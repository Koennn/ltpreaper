package me.koenn.reaper.util;

import me.koenn.reaper.LTPReaper;
import me.koenn.reaper.reaper.Pylon;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.ArrayList;
import java.util.List;

public class NPCUtil {

    public static List<LivingEntity> spawnBlazes(List<Pylon> pylons, int duration) {
        List<LivingEntity> blazes = new ArrayList<>();
        List<Integer> tasks = new ArrayList<>();

        for (Pylon pylon : pylons) {

            Location location = pylon.getLocation().clone();
            Blaze npc = spawnNPC(location, duration);
            blazes.add(npc);

            tasks.add(Bukkit.getScheduler().scheduleSyncRepeatingTask(LTPReaper.getInstance(), () -> {

                location.add(0, 0.016, 0);
                npc.setFireTicks(0);
                npc.setTarget(null);
                npc.setHealth(20);
                npc.teleport(location, PlayerTeleportEvent.TeleportCause.PLUGIN);

            }, 0, 1));
        }

        Bukkit.getScheduler().scheduleSyncDelayedTask(LTPReaper.getInstance(), () -> {

            for (int task : tasks) {
                Bukkit.getScheduler().cancelTask(task);
            }

            blazes.forEach(LivingEntity::remove);

        }, duration);

        return blazes;
    }

    private static Blaze spawnNPC(Location location, int duration) {
        Blaze blaze = (Blaze) location.getWorld().spawnEntity(location, EntityType.BLAZE);
        blaze.setFireTicks(0);
        blaze.setTarget(null);
        blaze.setNoDamageTicks(duration);
        blaze.setHealth(20);
        return blaze;
    }
}
