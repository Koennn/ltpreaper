package me.koenn.reaper.util;

import me.koenn.core.misc.FancyString;
import me.koenn.reaper.LTPReaper;
import me.koenn.reaper.reaper.Pylon;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import static me.koenn.reaper.references.Messages.*;
import static org.bukkit.ChatColor.translateAlternateColorCodes;

public class ReaperToolUtil {

    public static void changeMode(Player player, Mode mode, ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        String message = translateAlternateColorCodes('&', PREFIX + MODE_SET);
        switch (mode) {
            case PYLON:
                meta.setLore(Util.getLore(Mode.FIRE));
                player.sendMessage(message.replace("{mode}", new FancyString(Mode.FIRE).toString()));
                break;
            case REAPER:
                meta.setLore(Util.getLore(Mode.PYLON));
                player.sendMessage(message.replace("{mode}", new FancyString(Mode.PYLON).toString()));
                break;
            case FIRE:
                meta.setLore(Util.getLore(Mode.REAPER));
                player.sendMessage(message.replace("{mode}", new FancyString(Mode.REAPER).toString()));
                break;
        }
        item.setItemMeta(meta);
    }

    public static void executeMode(Player player, Location location, Mode mode) {
        String locationString = location.getX() + ", " + location.getY() + ", " + location.getZ();
        switch (mode) {
            case PYLON:
                if (LTPReaper.reaper.containsPylon(new Pylon(location))) {
                    return;
                }
                LTPReaper.reaper.addPylon(new Pylon(location));
                player.sendMessage(translateAlternateColorCodes('&', PREFIX + ADDED_PYLON).replace("{location}", locationString));
                break;
            case REAPER:
                if (LTPReaper.reaper.getLocation().equals(location)) {
                    return;
                }
                LTPReaper.reaper.setLocation(location);
                player.sendMessage(translateAlternateColorCodes('&', PREFIX + SET_REAPER).replace("{location}", locationString));
                break;
            case FIRE:
                if (LTPReaper.reaper.containsFireplace(location)) {
                    return;
                }
                LTPReaper.reaper.addFireplace(location);
                player.sendMessage(translateAlternateColorCodes('&', PREFIX + ADDED_FIREPLACE).replace("{location}", locationString));
                break;
        }
    }
}
