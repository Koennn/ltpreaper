package me.koenn.reaper.util;

import me.koenn.core.cgive.CItem;
import me.koenn.reaper.soulgem.SoulGem;
import me.koenn.reaper.soulgem.SoulGemType;
import org.bukkit.inventory.ItemStack;

public class CItems {

    public static CItem reaperTool() {
        return new CItem() {
            @Override
            public ItemStack getItem() {
                return Util.getReaperTool();
            }

            @Override
            public String getName() {
                return "reapertool";
            }
        };
    }

    public static CItem soulGem(SoulGemType type) {
        return new CItem() {
            @Override
            public ItemStack getItem() {
                return new SoulGem(type);
            }

            @Override
            public String getName() {
                return "soulgem_" + type.name().toLowerCase();
            }
        };
    }
}
