package me.koenn.reaper;

import me.koenn.core.cgive.CGiveAPI;
import me.koenn.core.command.CommandAPI;
import me.koenn.core.config.JSONManager;
import me.koenn.reaper.commands.TokensCommand;
import me.koenn.reaper.listeners.ClickListener;
import me.koenn.reaper.listeners.DragDropListener;
import me.koenn.reaper.listeners.PlayerDeathListener;
import me.koenn.reaper.listeners.PlayerPickupItemListener;
import me.koenn.reaper.reaper.Reaper;
import me.koenn.reaper.soulgem.SoulGemType;
import me.koenn.reaper.util.CItems;
import me.koenn.reaper.util.ParticleUtil;
import me.koenn.reaper.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class LTPReaper extends JavaPlugin {

    public static Reaper reaper;
    private static JSONManager jsonManager;
    /**
     * Static Plugin instance.
     */
    private static Plugin instance;

    public static JSONManager getJsonManager() {
        return jsonManager;
    }

    /**
     * Get the current Plugin instance.
     *
     * @return Plugin instance
     */
    public static Plugin getInstance() {
        return instance;
    }

    /**
     * Simple logger method to log a message to the console.
     *
     * @param msg Message to log
     */
    public void log(String msg) {
        this.getLogger().info(msg);
    }

    @Override
    public void onEnable() {
        //Set the current Plugin instance.
        instance = this;

        //Send credit message.
        log("All credits for this plugin go to Koenn");

        if (!this.getDataFolder().exists()) {
            this.getDataFolder().mkdir();
        }

        //Register Bukkit event Listeners.
        Bukkit.getPluginManager().registerEvents(new ClickListener(), this);
        Bukkit.getPluginManager().registerEvents(new DragDropListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerDeathListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerPickupItemListener(), this);

        //Register Commands.
        CommandAPI.registerCommand(new TokensCommand(), this);

        //Register JSONManager.
        jsonManager = new JSONManager(this, "reaper.json");

        //Register CItems.
        CGiveAPI.registerCItem(CItems.reaperTool(), this);
        CGiveAPI.registerCItem(CItems.soulGem(SoulGemType.SIMPLE), this);
        CGiveAPI.registerCItem(CItems.soulGem(SoulGemType.RARE), this);
        CGiveAPI.registerCItem(CItems.soulGem(SoulGemType.LEGENDARY), this);

        reaper = Util.loadReaper();
        if (reaper == null) {
            reaper = new Reaper();
        }

        ParticleUtil.startPassiveParticle();
    }

    @Override
    public void onDisable() {
        log("All credits for this plugin go to Koenn");
    }
}