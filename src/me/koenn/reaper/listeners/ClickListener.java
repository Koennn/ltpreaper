package me.koenn.reaper.listeners;

import me.koenn.reaper.LTPReaper;
import me.koenn.reaper.reaper.Reaper;
import me.koenn.reaper.util.Mode;
import me.koenn.reaper.util.ReaperToolUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import static org.bukkit.ChatColor.WHITE;

public class ClickListener implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (!player.isOp()) {
            return;
        }

        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && !event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            return;
        }

        Reaper reaper = LTPReaper.reaper;
        if (reaper == null) {
            reaper = new Reaper();
        }

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && player.getItemInHand() != null) {
            if (event.getClickedBlock().getLocation().add(0.5, 1.5, 0.5).equals(reaper.getLocation())) {
                reaper.activate(player);
            }
        }

        if (player.getItemInHand() == null || !player.getItemInHand().getType().equals(Material.BLAZE_ROD)) {
            return;
        }

        ItemStack item = player.getItemInHand();
        if (!item.getItemMeta().hasDisplayName() || !item.getItemMeta().hasLore() || !item.getItemMeta().getDisplayName().equals(WHITE + "Reaper Tool")) {
            return;
        }

        Mode mode = Mode.valueOf(item.getItemMeta().getLore().get(0).split(" ")[1]);
        if (event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            ReaperToolUtil.changeMode(player, mode, item);
            return;
        }

        Location location = event.getClickedBlock().getLocation().add(0.5, 1.5, 0.5);
        ReaperToolUtil.executeMode(player, location, mode);
    }
}
