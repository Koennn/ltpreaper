package me.koenn.reaper.listeners;

import me.koenn.core.events.DragDropEvent;
import me.koenn.core.misc.LoreHelper;
import me.koenn.reaper.references.ItemReferences;
import me.koenn.reaper.soulgem.SoulGem;
import me.koenn.reaper.util.ParticleUtil;
import me.koenn.reaper.util.Util;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class DragDropListener implements Listener {

    @EventHandler
    public void onDragDrop(DragDropEvent event) {
        if (!event.getCursor().getType().equals(Material.EMERALD)) {
            return;
        }
        ItemStack gem = event.getCursor();
        if (!gem.getItemMeta().hasDisplayName() || !gem.getItemMeta().hasLore()) {
            return;
        }

        if (!Util.isValidWeapon(event.getItem().getType())) {
            return;
        }

        SoulGem soulGem;
        try {
            soulGem = new SoulGem(gem);
        } catch (IllegalArgumentException ex) {
            return;
        }

        event.setHandled(true);
        ItemMeta meta = event.getItem().getItemMeta();
        meta.setLore(LoreHelper.makeLore(ItemReferences.SOUL_GEM_KILLS
                .replace("{color}", soulGem.getGemType().getColor())
                .replace("{kills}", String.valueOf(0))));
        event.getItem().setItemMeta(meta);
        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.LEVEL_UP, 1.0F, 0.5F);
        ParticleUtil.playApplyEffect(event.getPlayer());
    }
}
