package me.koenn.reaper.listeners;

import com.connorlinfoot.actionbarapi.ActionBarAPI;
import me.koenn.core.misc.ColorHelper;
import me.koenn.reaper.references.Messages;
import me.koenn.reaper.util.Util;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerDeathListener implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player killer = event.getEntity().getKiller();
        ItemStack item = killer.getItemInHand();
        if (item == null) {
            return;
        }
        if (!Util.isValidWeapon(item.getType()) || !Util.hasSoulGem(item)) {
            return;
        }

        try {
            Util.setKills(item, Util.getKills(item) + 1);
        } catch (NumberFormatException ex) {
            return;
        }

        ActionBarAPI.sendActionBar(killer, ColorHelper.readColor(Messages.NEW_KILL));
        killer.playSound(killer.getLocation(), Sound.ENDERDRAGON_HIT, 1.0F, 1.3F);
    }
}
