package me.koenn.reaper.references;

public class ItemReferences {

    public static final String SOUL_GEM_NAME = "{type} Soul Gem";
    public static final String SOUL_GEM_LORE = "&7Drag n` Drop onto weapon to apply.";
    public static final String SOUL_GEM_KILLS = "{color}Kills: {kills}";
}
