package me.koenn.reaper.references;

public class Messages {

    public static final String PREFIX = "&9Reaper > ";

    public static final String ADDED_PYLON = "&7Added pylon at &e{location}";
    public static final String ADDED_FIREPLACE = "&7Added fireplace at &e{location}";
    public static final String SET_REAPER = "&7Set reaper location to &e{location}";
    public static final String MODE_SET = "&7Mode set to &e{mode}";
    public static final String NEW_KILL = "&a&l+1 Kill!";
    public static final String TOKENS = "&7You have &e{tokens} &7tokens!";
    public static final String EARNED = "&a&lYou earned &e&l{tokens} &a&lreaper tokens!";
}
