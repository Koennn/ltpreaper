package me.koenn.reaper.reaper;

import com.connorlinfoot.actionbarapi.ActionBarAPI;
import me.koenn.core.misc.LocationHelper;
import me.koenn.core.misc.Timer;
import me.koenn.core.player.CPlayer;
import me.koenn.core.player.CPlayerRegistry;
import me.koenn.reaper.LTPReaper;
import me.koenn.reaper.references.Messages;
import me.koenn.reaper.soulgem.SoulGemType;
import me.koenn.reaper.util.NPCUtil;
import me.koenn.reaper.util.ParticleUtil;
import me.koenn.reaper.util.Util;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.json.simple.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class Reaper {

    private final List<Pylon> pylons;
    private final List<Location> firePlaces;
    private Location location;
    private Player user;
    private ItemStack item;

    public Reaper() {
        this.pylons = new ArrayList<>();
        this.firePlaces = new ArrayList<>();
        this.user = null;
        this.item = null;
    }

    public Reaper(Location location, List<Pylon> pylons, List<Location> firePlaces) {
        this.pylons = pylons;
        this.firePlaces = firePlaces;
        this.location = location;
        this.user = null;
        this.item = null;
    }

    public void activate(Player player) {
        if (this.user == null) {
            ItemStack item = player.getItemInHand();
            if (!Util.isValidWeapon(item.getType()) || !Util.hasSoulGem(item) || Util.getKills(item) == 0) {
                return;
            }

            ItemMeta meta = item.getItemMeta();
            meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
            item.setItemMeta(meta);
            this.item = item;
            this.user = player;
            player.setItemInHand(new ItemStack(Material.AIR));
            this.start(300, player);
        }
    }

    private void start(int duration, Player player) {
        this.location.getWorld().playSound(this.location, Sound.BLAZE_BREATH, 2.0F, 2.0F);

        ParticleUtil.playBeams(duration, this.location, NPCUtil.spawnBlazes(this.pylons, duration));
        firePlaces.forEach(firePlace -> firePlace.getBlock().setType(Material.FIRE));
        this.sendProgressBar(duration, player);

        Item item = this.location.getWorld().dropItem(this.location, this.item);
        item.setVelocity(item.getVelocity().zero());

        new Timer(duration, LTPReaper.getInstance()).start(() -> {
            this.location.getWorld().playSound(this.location, Sound.WITHER_DEATH, 1.0F, 1.0F);
            firePlaces.forEach(firePlace -> firePlace.getBlock().setType(Material.AIR));
            item.remove();

            SoulGemType gemType = SoulGemType.fromColor(this.item.getItemMeta().getLore().get(0));
            int tokens = Util.getKills(this.item) * gemType.getMultiplier();
            Util.setKills(this.item, 0);

            if (this.user.isOnline()) {
                this.user.getInventory().addItem(this.item);
            }

            CPlayer cPlayer = CPlayerRegistry.getCPlayer(player.getUniqueId());
            if (!cPlayer.hasField("reapertokens")) {
                cPlayer.set("reapertokens", tokens);
            } else {
                cPlayer.set("reapertokens", Integer.parseInt(cPlayer.get("reapertokens")) + tokens);
            }
            cPlayer.sendMessage(Messages.PREFIX + Messages.EARNED.replace("{tokens}", String.valueOf(tokens)));

            this.user = null;
            ParticleUtil.startPassiveParticle();
        });
    }

    private void sendProgressBar(int duration, Player player) {
        for (int i = duration / 10; i > 0; i--) {

            int maxLength = (duration / 10) + 2;
            char[] progress = new char[maxLength];
            progress[0] = '§';
            progress[1] = 'a';
            for (int x = 2; x < i; x++) {
                progress[x] = '|';
            }
            progress[i] = '§';
            progress[i + 1] = 'c';
            for (int x = i + 2; x < maxLength; x++) {
                progress[x] = '|';
            }

            String m = "";
            for (char c : progress) {
                m += c;
            }
            String message = m;

            new Timer(i * 10, LTPReaper.getInstance()).start(() -> ActionBarAPI.sendActionBar(player, ChatColor.RED + message));
        }
        new Timer(duration + 10, LTPReaper.getInstance()).start(() -> ActionBarAPI.sendActionBar(player, ""));
    }

    public boolean containsFireplace(Location fireplace) {
        for (Location f : this.firePlaces) {
            if (f.equals(fireplace)) {
                return true;
            }
        }
        return false;
    }

    public boolean containsPylon(Pylon pylon) {
        for (Pylon p : this.pylons) {
            if (p.equals(pylon)) {
                return true;
            }
        }
        return false;
    }

    public Location getLocation() {
        if (location == null) {
            return new Location(Bukkit.getServer().getWorlds().get(0), 0, 0, 0);
        }
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
        LTPReaper.getJsonManager().setInBody("location", LocationHelper.getString(location));
    }

    public void addPylon(Pylon pylon) {
        this.pylons.add(pylon);

        JSONArray pylons = (JSONArray) LTPReaper.getJsonManager().getFromBody("pylons");
        if (pylons == null) {
            pylons = new JSONArray();
        }

        pylons.add(LocationHelper.getString(pylon.getLocation()));
        LTPReaper.getJsonManager().setInBody("pylons", pylons);
    }

    public void addFireplace(Location location) {
        this.firePlaces.add(location);

        JSONArray fireplaces = (JSONArray) LTPReaper.getJsonManager().getFromBody("fireplaces");
        if (fireplaces == null) {
            fireplaces = new JSONArray();
        }

        fireplaces.add(LocationHelper.getString(location));
        LTPReaper.getJsonManager().setInBody("fireplaces", fireplaces);
    }

    public boolean isInUse() {
        return this.user != null;
    }
}
