package me.koenn.reaper.reaper;

import org.bukkit.Location;

public class Pylon {

    private final Location location;

    public Pylon(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Pylon) {
            return this.getLocation().equals(((Pylon) obj).getLocation());
        }
        return super.equals(obj);
    }
}
